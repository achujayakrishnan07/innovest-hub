const express = require('express');

const mysql = require('mysql');

const app = express();

const port= 3000;

const pool = mysql.createPool({
    connectionLimit : 10,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'innovest'
});

app.get('/',(request,response) => {

    response.sendFile(__dirname + '/index.html');

});

app.get('/search',(request,response) => {

    const query = request.query.q;

    var sql = '';

    if(query != '')
	{
		sql = `SELECT * FROM fundingscheme WHERE Scheme_id LIKE '%${query}%' OR Fund_type LIKE '%${query}%' OR Product_phase LIKE '%${query}%'`;
	}
	else
	{
		sql = `SELECT * FROM fundingscheme ORDER BY Scheme_id`;
	}

	pool.query(sql, (error, results) => {

		if (error) throw error;

		response.send(results);

	});

});

app.listen(port, () => {

	console.log(`Server listening on port ${port}`);

});
