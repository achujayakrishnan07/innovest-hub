const express = require('express');

const mysql = require('mysql');

const app = express();

const port= 3002;

const pool = mysql.createPool({
    connectionLimit : 10,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'innovest'
});

app.get('/',(request,response) => {

    response.sendFile(__dirname + '/index2.html');

});

app.get('/search',(request,response) => {

    const query = request.query.q;

    var sql = '';

    if(query != '')
	{
		sql = `SELECT * FROM innovator WHERE innovator_id LIKE '%${query}%' OR innovator_name LIKE '%${query}%'`;
	}
	else
	{
		sql = `SELECT * FROM innovator ORDER BY innovator_id`;
	}

	pool.query(sql, (error, results) => {

		if (error) throw error;

		response.send(results);

	});

});

app.listen(port, () => {

	console.log(`Server listening on port ${port}`);

});
