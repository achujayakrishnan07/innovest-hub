const express = require('express');

const mysql = require('mysql');

const app = express();

const port= 3001;

const pool = mysql.createPool({
    connectionLimit : 10,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'innovest'
});

app.get('/',(request,response) => {

    response.sendFile(__dirname + '/index1.html');

});

app.get('/search',(request,response) => {

    const query = request.query.q;

    var sql = '';

    if(query != '')
	{
		sql = `SELECT * FROM startup WHERE startup_id LIKE '%${query}%' OR startup_name LIKE '%${query}%'`;
	}
	else
	{
		sql = `SELECT * FROM startup ORDER BY startup_id`;
	}

	pool.query(sql, (error, results) => {

		if (error) throw error;

		response.send(results);

	});

});

app.listen(port, () => {

	console.log(`Server listening on port ${port}`);

});
