const express = require('express');

const mysql = require('mysql');

const app = express();

const port= 3003;

const pool = mysql.createPool({
    connectionLimit : 10,
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'innovest'
});

app.get('/',(request,response) => {

    response.sendFile(__dirname + '/index3.html');

});

app.get('/search',(request,response) => {

    const query = request.query.q;

    var sql = '';

    if(query != '')
	{
		sql = `SELECT * FROM investor WHERE investor_id LIKE '%${query}%' OR investor_name LIKE '%${query}%'`;
	}
	else
	{
		sql = `SELECT * FROM investor ORDER BY investor_id`;
	}

	pool.query(sql, (error, results) => {

		if (error) throw error;

		response.send(results);

	});

});

app.listen(port, () => {

	console.log(`Server listening on port ${port}`);

});
